########################################################################################################################
#!!
#! @input string: Enter the string to get it's length
#!
#! @output length: Length of the input
#!!#
########################################################################################################################
namespace: Telefonica
flow:
  name: helloworld
  inputs:
    - string: "${get_sp('StringTests')}"
  workflow:
    - length:
        do:
          io.cloudslang.base.strings.length:
            - origin_string: '${string}'
        publish:
          - output: '${length}'
        navigate:
          - SUCCESS: validate
    - append:
        do:
          io.cloudslang.base.strings.append:
            - origin_string: '${string}'
            - text: '${string}'
        publish:
          - output_0: >-
              ${a = [13,5,21]
              b = a[1]}
        navigate:
          - SUCCESS: SUCCESS
    - validate:
        do:
          io.cloudslang.base.xml.validate:
            - xml_document: "${get_sp('xmlTest')}"
        navigate:
          - SUCCESS: append
          - FAILURE: on_failure
    - on_failure:
        - append_1:
            do:
              io.cloudslang.base.strings.append:
                - origin_string: '1 '
                - text: error
            publish:
              - error_message: '${new_string}'
  outputs:
    - length: '${output}'
    - error_message: '${error_message}'
  results:
    - SUCCESS
    - FAILURE
extensions:
  graph:
    steps:
      length:
        x: 100
        y: 150
      append:
        x: 400
        y: 150
        navigate:
          5eb10e64-2fb3-cf75-1f18-6a6e8cdda8bf:
            targetId: f7e4dcc7-4ace-1881-07e8-95f600976639
            port: SUCCESS
      validate:
        x: 269
        y: 44
    results:
      SUCCESS:
        f7e4dcc7-4ace-1881-07e8-95f600976639:
          x: 700
          y: 150
